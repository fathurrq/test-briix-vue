export type movie = {
  id: number;
  title: string;
  director: string;
  summary: string;
  genre: string[];
};
