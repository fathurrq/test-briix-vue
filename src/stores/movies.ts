import { defineStore } from 'pinia';

import { movie } from '@/types/movie';

export const useMoviesStore = defineStore('movies', {
  state: (): { movies: movie[] } => ({
    movies: [],
  }),
  getters: {
    getmovieById: (state) => {
      return (movieId: number) =>
        state.movies.find((movie) => movie.id === movieId);
    },
  },
  actions: {
    addMovie(data: movie) {
      this.movies = [...this.movies, data];
    },
    updateMovie(movieId: number, data: movie) {
      const movieIndex = this.movies.findIndex((movie) => movie.id === movieId);

      if (movieIndex !== -1) {
        this.movies[movieIndex] = {
          ...this.movies[movieIndex],
          ...data,
        };
      }
    },
    removeMovie(movieId: number) {
      this.movies = this.movies.filter((movie) => movie.id !== movieId);
    },
  },
});
